﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBehaviour : MonoBehaviour
{
	public static MonsterBehaviour monsterIntance;

	public float contaminationSpawnTime = 1f;
	public float tentacleSpawnTime = 1f;
	public float tentacleTime = 1f;

	public Transform[] tentacles;

    void Start()
    {
		monsterIntance = this;
		mTentacleTimer = Time.time + tentacleSpawnTime;
    }

    void Update()
    {
		if ( mTentacleTimer <= Time.time && !mTentacleActive )
		{
			SpawnTentacle ();
		}
		else if ( mTentacleTimer <= Time.time && mTentacleActive )
		{
			DespawnTentacle ();
		}
    }

	public void Feed(int type)
	{
		mPower += type;
	}

	private void SpawnTentacle()
	{
		mTentacleIndex = Random.Range ( 0 , 3 );
		Vector3 playerPos = PlayerMovement.playerInstance.transform.position;
		tentacles[mTentacleIndex].right = playerPos - tentacles[mTentacleIndex].position;
		Vector3 rot = tentacles[mTentacleIndex].eulerAngles;
		rot.z -= 90;
		tentacles[mTentacleIndex].eulerAngles = rot;
		tentacles[mTentacleIndex].gameObject.SetActive ( true );
		mTentacleActive = true;
		mTentacleTimer = Time.time + tentacleTime;
	}

	private void DespawnTentacle()
	{
		tentacles[mTentacleIndex].gameObject.SetActive ( false );
		mTentacleActive = false;
		mTentacleTimer = Time.time + tentacleSpawnTime;
	}

	public void Contaminate()
	{

	}

	private float mTentacleTimer = 0.0f;
	private bool mTentacleActive = false;
	private int mTentacleIndex = -1;
	private int mPower = 0;
}
