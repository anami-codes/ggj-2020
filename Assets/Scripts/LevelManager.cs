﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    [Header("Enemies")]
	public float velocityStep = 1f;
    public float spawnTime = 1.0f;
	public Transform[] spawnPoints;
	public GameObject[] enemies;

    [Header("Damage - Black Shadow")]
    public Animator animDamage;
    private bool isShadowActive = false;

    [Header("Boss")]
    public bool isBossAttacking = false;
    public GameObject boss;


    public static float levelSpeed
	{
		get { return mSpeed; }
	}


    void Awake()
    {
        Instance = this;
		mSpawnTimer = Time.time + 1f;
    }
	
    void Update()
    {
		if ( mTimer <= Time.time )
		{
			mSpeed += 0.001f;
			if (spawnTime > 0.25f) spawnTime -= 0.1f;
			mTimer = Time.time + velocityStep;
		}

		if ( mSpawnTimer <= Time.time )
			Spawn ();
    }

	private void Spawn()
	{
		int point = Random.Range ( 0 , 4 );
		int enemy = Random.Range ( 0 , 3 );
		Instantiate ( enemies[enemy] , spawnPoints[point].position , spawnPoints[point].rotation );
		mSpawnTimer = Time.time + spawnTime;
	}

    public void DamageHealed()
    {
        if (levelPoints < fullPoints && isShadowActive)
        {
            animDamage.SetTrigger("Heal");
            levelPoints += pointHeal;
        }
        else
        {
            if (levelPoints == fullPoints) isShadowActive = false;
        }
    }

    public void DamageRecieved()
    {
        if (levelPoints >= 0)
        {
            levelPoints -= pointDamage;

            if (levelPoints == 0)
            {
                animDamage.SetTrigger("Damage");
                Debug.Log("Boss Attack!");
                isBossAttacking = true;
                //boss.SetActive(true);
            }
            else if (levelPoints == 10 || levelPoints == 30 || levelPoints == 50 || levelPoints == 70 || levelPoints == 90)
            {
                animDamage.SetTrigger("Damage");
                isShadowActive = true;
                Debug.Log("Damage");
            }
        }


    }

	private float mSpawnTimer = 0.0f;
	private float mTimer = 0.0f;
    private int fullPoints = 100;
    private int levelPoints = 100;
    private int pointDamage = 5;
    private int pointHeal = 5;
    private int healing = 0;
    private static float mSpeed = 0f;
}
