﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightMovement : MonoBehaviour
{
    public static LightMovement Instance;
    private Animator anim;
    private Quaternion lookRotation;
    private Vector3 direction;
    private float angle;

    void Start()
    {
        Instance = this;
        anim = GetComponent<Animator>();
    }

    public void Attack(Vector3 targetPosition)
    {
        anim.SetTrigger("Attack");
        direction = targetPosition - transform.position;
        angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        lookRotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = lookRotation;
        //transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10);
    }

    public void StopAttack()
    {
        anim.SetTrigger("StopAttack");

    }
}
