﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
	public int life;
    private Animator anim_virus;

    void Start()
    {
        anim_virus = transform.GetChild(0).GetComponent<Animator>();
    }
	
    void Update()
    {
		transform.position -= Vector3.right * ( (LevelManager.levelSpeed * 20f )+ 1f) * Time.deltaTime;
    }

	public void Damage()
	{
		life--;
        anim_virus.SetTrigger("Heal");

		if (life <= 0)
		{
			PlayerMovement.playerInstance.EndAttack ();
            Destroy( gameObject );
        }
	}

	private void OnTriggerEnter2D( Collider2D collision )
	{
		if ( collision.name == "DeathZone" )
		{
            LevelManager.Instance.DamageRecieved();
			//MonsterBehaviour.monsterIntance.Feed (1);
			Destroy ( gameObject );
            Debug.Log("Destroy");
		}
	}
}
