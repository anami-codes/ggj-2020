﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler
{
    public bool isInputInFrontOfPlayer = false;

	public enum Action
	{
		Move,
		Attack,
		Dodge,
		None,
	}

	public Vector2 currentPosition
	{
		get { return mCurrentScreenPos; }
	}

	public Action currentAction
	{
		get { return mCurrentAction; }
	}

	public EnemyBehaviour currentTarget
	{
		get { return mEnemy; }
	}

	public InputHandler()
	{
		mIsComputer = ( Application.platform == RuntimePlatform.WindowsEditor );
	}

	public void UpdateInput()
	{
		if ( !mIsComputer && Input.touchCount > 0 )
		{
			if ( !mTouchStarted && Input.GetTouch ( 0 ).phase == TouchPhase.Ended )
			{
                if (IsSwipe())
                    mCurrentAction = Action.Dodge;
                else
                {
                    HasTarget();
                    mCurrentAction = Action.Attack;
                    //mCurrentAction = ( HasTarget () ) ? Action.Attack : Action.Move;
                }
            }
			else
			{
                
				StartTouch ();
				mCurrentAction = Action.None;
			}
		}
		else if ( mIsComputer )
		{
			if ( Input.GetMouseButtonDown ( 0 ) )
			{
				mCurrentScreenPos = Input.mousePosition;
                if (PlayerMovement.playerInstance.transform.position.x < Camera.main.ScreenToWorldPoint(mCurrentScreenPos).x)
                {
                    isInputInFrontOfPlayer = true;
                }
                else
                {
                    isInputInFrontOfPlayer = false;
                }
                HasTarget();
                mCurrentAction = Action.Attack;
				//mCurrentAction = ( HasTarget () ) ? Action.Attack : Action.Move;
            }
			else if ( Input.GetMouseButtonDown ( 1 ) )
			{
				mCurrentAction = Action.Dodge;
			}
			else
			{
				mCurrentAction = Action.None;
            }
        }
		else
		{
			mCurrentAction = Action.None;
		}
	}

	private void StartTouch()
	{
		mTouchStarted = Input.GetTouch ( 0 ).phase == TouchPhase.Began;
		mCurrentScreenPos = Input.GetTouch ( 0 ).position;
        if (PlayerMovement.playerInstance.transform.position.x < Camera.main.ScreenToWorldPoint(mCurrentScreenPos).x)
            isInputInFrontOfPlayer = true;
        else
            isInputInFrontOfPlayer = false;
    }

	private bool IsSwipe()
	{
		Vector2 newScreenPos = Input.GetTouch ( 0 ).position;
		Vector2 dir = currentPosition - newScreenPos;
		dir.Normalize ();
		return ( ( dir.y >= dir.x ) && ( dir.y > 0.1f ) );
	}

	private bool HasTarget()
	{
		RaycastHit2D hit = Physics2D.Raycast ( Camera.main.ScreenToWorldPoint ( currentPosition ) , Vector3.forward , 100f );
		if ( hit )
			mEnemy = hit.transform.GetComponent<EnemyBehaviour> ();

		return ( hit && mEnemy != null );
	}

	private EnemyBehaviour mEnemy;
	private Vector2 mCurrentScreenPos = Vector2.zero;
	public Action mCurrentAction = Action.None;
	private bool mTouchStarted = false;

	private bool mIsComputer = true;
}
