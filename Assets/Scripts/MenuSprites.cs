﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSprites : MonoBehaviour
{
    void Start()
    {
		speed = Random.Range ( 0.001f , 0.05f );
    }
	
    void Update()
    {
		transform.Translate ( Vector2.left * speed );

		if ( transform.position.x <= -12 )
		{
			Vector3 pos = transform.position;
			pos.x = 12;
			transform.position = pos;
			speed = Random.Range ( 0.001f , 0.1f );
		}
    }

	float speed;
}
