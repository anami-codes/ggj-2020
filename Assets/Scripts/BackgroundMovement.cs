﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour
{
    private Transform[] backgrounds;
    public float speed;
    public Transform pointEnd;
    public Transform pointStart;
    private Vector3 pos;

    void Start()
    {
        backgrounds = new Transform[transform.childCount];
        for(int i=0;i<transform.childCount;i++)
        {
            backgrounds[i] = transform.GetChild(i);
        }


    }

    void Update()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            pos = backgrounds[i].transform.position;
            pos.x -= speed * LevelManager.levelSpeed * Time.deltaTime;
            if (pos.x <= pointEnd.position.x)
            {
                pos.x = pointStart.position.x;
            }
            backgrounds[i].transform.position = pos;
        }
    }
}
