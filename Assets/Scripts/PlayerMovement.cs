﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public static PlayerMovement playerInstance;
    private InputHandler.Action currentAction;

	public float speed = 1.0f;
	public float dodgeTime = 0.25f;

    void Start()
    {
		playerInstance = this;
		controller = new InputHandler ();
		collider = GetComponent<CircleCollider2D> ();
    }
	
    void Update()
    {
		controller.UpdateInput();
		currentAction = controller.currentAction;

		switch ( currentAction )
		{
			case InputHandler.Action.Move:
				//if ( !attacking )
				//{
				//	target = Camera.main.ScreenToWorldPoint ( controller.currentPosition );
				//	target.z = 0;
				//	SetDirection ();
				//}
				//else
				//	Attack ();
				break;
			case InputHandler.Action.Attack:
                //if ( !attacking && controller.currentTarget != null)
                //{
                //	target = controller.currentTarget.transform.position;
                //	SetDirection ();
                //	movingToEnemy = true;
                //}
                //else
                if (!LevelManager.Instance.isBossAttacking)
                {
                    if (controller.currentTarget != null)
                        Attack();
                }
                else
                {
                    target = Camera.main.ScreenToWorldPoint(controller.currentPosition);
                    target.z = 0;
                    SetDirection();
                    //movingToEnemy = true;
                }
                break;
			case InputHandler.Action.Dodge:
				if ( !minvincible && mTimer <= Time.time )
				{
					minvincible = true;
					mTimer = Time.time + dodgeTime;
					collider.isTrigger = true;
				}
				break;
		}

		if ( minvincible && mTimer <= Time.time )
		{
			minvincible = false;
			mTimer = Time.time + dodgeTime;
			collider.isTrigger = false;
		}

		if ( ( attacking || movingToEnemy ) && controller.currentTarget == null )
		{
			movingToEnemy = false;
			attacking = false;
			target = Vector3.zero;
			SetDirection ();
		}

			if ( attacking )
			MoveBackwards();
		else
			Move ();
    }

	private void SetDirection()
	{
		Vector3 pos = transform.position;
		dir = target - pos;
		dir.z = 0;
		dir.Normalize ();
	}

	private void Move()
	{
		float targetDis = ( movingToEnemy ) ? 0.2f : 0.01f;

		if ( Vector3.Distance ( transform.position , target ) < targetDis )
		{
			target = transform.position;
			if ( movingToEnemy )
			{
				attacking = true;
				movingToEnemy = false;
				Attack ();
			}
		}
		else
		{
			if ( movingToEnemy )
			{
				if ( controller.currentTarget != null )
				{
					target = controller.currentTarget.transform.position;
					SetDirection ();
				}
				
			}

			transform.position += dir * speed * Time.deltaTime;
		}
	}

    private void Attack()
    {
        if (controller.isInputInFrontOfPlayer) LightMovement.Instance.Attack(controller.currentTarget.transform.position);
        controller.currentTarget.Damage();
	}

	private void MoveBackwards()
	{
		transform.position -= Vector3.right * LevelManager.levelSpeed * Time.time;
	}

	public void EndAttack()
	{
		attacking = false;
        LightMovement.Instance.StopAttack();
        LevelManager.Instance.DamageHealed();
    }

	private void OnTriggerEnter2D( Collider2D collision )
	{
        if (collision.tag == "Enemy")
        {
            Debug.Log("damaging enemy");
            life--;
        }
	}

	private InputHandler controller;
	Vector3 dir = Vector3.zero;
	Vector3 target = Vector3.zero;
	bool movingToEnemy = false;
	bool attacking = false;
	private int life = 10;
	private float mTimer = 0.0f;
	private bool minvincible = false;
	private CircleCollider2D collider;
}
