﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public float scrollSpeedWhite = 1.0f;
	public float scrollSpeedBlack = 1.0f;

	public MeshRenderer whiteSpots;
	public MeshRenderer blackSpots;
	public MeshRenderer lines;

	public GameObject menuUI;
	public GameObject menuBackground;
	public GameObject creditsUI;

	private void Update()
	{
		offset_1 += Vector2.right * scrollSpeedWhite * Time.deltaTime;
		whiteSpots.material.SetTextureOffset ( "_MainTex" , offset_1 );

		offset_2 += Vector2.right * scrollSpeedBlack * Time.deltaTime;
		blackSpots.material.SetTextureOffset ( "_MainTex" , offset_2 );

		t += Time.deltaTime;
		offset_3 = Mathf.Lerp ( 0.05f , 0.5f, Mathf.Abs( Mathf.Sin ( t ) ) );
		lines.material.SetFloat ( "_Cutoff" , offset_3 );
	}

	public void Play()
	{
		SceneManager.LoadScene ( 1 );
	}

	public void Exit()
	{
		Debug.Log ( "Exit" );
		Application.Quit ();
	}

	public void Credits()
	{
		menuUI.SetActive ( false );
		creditsUI.SetActive ( true );
	}

	public void EndCredits()
	{
		menuUI.SetActive ( true );
		creditsUI.SetActive ( false );
	}

	Vector2 offset_1 = Vector2.zero;
	Vector2 offset_2 = Vector2.zero;
	float offset_3 = 0.01f;
	float t = 0.0f;
}
